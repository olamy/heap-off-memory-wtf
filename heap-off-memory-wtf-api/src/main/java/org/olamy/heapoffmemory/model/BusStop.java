package org.olamy.heapoffmemory.model;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Olivier Lamy
 */
@XmlRootElement( name = "busStop" )
public class BusStop
    implements Serializable
{


    public String id;

    public String type;

    public String name;

    public String inseeCode;

    // OUI / NON / PARTIEL
    public String handicappedAccess;

    public List<String> lines = new ArrayList<String>();

    public String furnitures;

    public boolean seating;

    public boolean light;

    public boolean trash;


    @Override
    public String toString()
    {
        final StringBuilder sb = new StringBuilder();
        sb.append( "BusStop" );
        sb.append( "{id='" ).append( id ).append( '\'' );
        sb.append( ", type='" ).append( type ).append( '\'' );
        sb.append( ", name='" ).append( name ).append( '\'' );
        sb.append( ", inseeCode='" ).append( inseeCode ).append( '\'' );
        sb.append( ", handicappedAccess='" ).append( handicappedAccess ).append( '\'' );
        sb.append( ", lines=" ).append( lines );
        sb.append( ", furnitures='" ).append( furnitures ).append( '\'' );
        sb.append( ", seating=" ).append( seating );
        sb.append( ", light=" ).append( light );
        sb.append( ", trash=" ).append( trash );
        sb.append( '}' );
        return sb.toString();
    }
}
