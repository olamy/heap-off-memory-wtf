package org.olamy.heapoffmemory.parser;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import au.com.bytecode.opencsv.CSVReader;
import org.apache.commons.lang3.BooleanUtils;
import org.olamy.heapoffmemory.model.BusStop;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * @author Olivier Lamy
 */
public class DataParser
{
    private Logger log = LoggerFactory.getLogger( getClass() );

    // http://www.data.rennes-metropole.fr/les-donnees/catalogue/

    //TIMEO;TYPE    ;NOM         ;C_INSEE;ACCES_PMR;LI_ACCESS;LI_NORM;MOBILIER   ;BANC;ECLAIRAGE;POUBELLE
    //1001 ;mobilier;Longs Champs;35238  ;OUI      ;1        ;       ;Abri double;OUI ;OUI      ;NON

    /*
    TIMEO : identifiant de l'arrÍt physique
    TYPE : type de point : saeiv (dans la voie) / mobilier (sur le trottoir)
    NOM : nom commercial de l'arrÍt
    C_INSEE : code insee de la commune de rattachement
    ACCESS_PMR : information sur l'accessibilitÈ aux personnes ‡ mobilitÈ rÈduite (OUI / NON / PARTIEL)
    LI_ACCESS : liste des lignes accessibles aux personnes ‡ mobilitÈ rÈduite desservant cet arrÍt
    LI_NORM : liste des lignes non-accessibles desservant cet arrÍt
    MOBILIER : type de mobilier de l'arrÍt (poteau / abribus)
    BANC : indique si l'arrÍt est ÈquipÈ d'un banc
    ECLAIRAGE : indique si l'arrÍt est dotÈ d'un Èclairage
    POUBELLE : indique si l'arrÍt est ÈquipÈ d'une poubelle
    */

    public DataParser()
    {
        // no op
    }


    public List<BusStop> loadAll()
        throws IOException
    {
        List<BusStop> busStops = new ArrayList<BusStop>();

        InputStream is =
            Thread.currentThread().getContextClassLoader().getResourceAsStream( "star_arret_physique.csv" );

        CSVReader csvReader = new CSVReader( new InputStreamReader( is ), ';' );

        List<String[]> lines = csvReader.readAll();

        int counter = 0;

        for ( String[] line : lines )
        {
            if ( counter == 0 )
            {
                counter++;
                continue;
            }
            BusStop busStop = new BusStop();
            busStops.add( busStop );
            log.debug( "line {}", Arrays.asList( line ) );
            // 0     1    2   3       4      5      6       7       8         9         10      11       12   13        14
            //TIMEO;TYPE;NOM;C_INSEE;X_CC48;Y_CC48;X_WGS84;Y_WGS84;ACCES_PMR;LI_ACCESS;LI_NORM;MOBILIER;BANC;ECLAIRAGE;POUBELLE
            //1001;mobilier;Longs Champs;35238;1355395,088800000000000;1355395,088800000000000;-1,632530197157740;48,129076710012299;OUI;1;;Abri double;OUI;OUI;NON
            busStop.id = line[0];
            busStop.type = line[1];
            busStop.name = line[2];
            busStop.inseeCode = line[3];
            busStop.handicappedAccess = line[8];

            busStop.furnitures = line[11];
            busStop.seating = BooleanUtils.toBoolean( line[12] );
            busStop.light = BooleanUtils.toBoolean( line[13] );
            busStop.trash = BooleanUtils.toBoolean( line[14] );
            busStop.lines = generateLines();
            counter++;

        }

        return busStops;
    }

    private List<String> generateLines()
    {
        List<String> lines = new ArrayList<String>();

        for ( int i = 0; i < 100; i++ )
        {
            lines.add( Long.toString( new Date().getTime() ) );
        }

        return lines;
    }
}
