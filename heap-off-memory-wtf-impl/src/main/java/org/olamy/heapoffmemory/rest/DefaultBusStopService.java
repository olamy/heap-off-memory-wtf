package org.olamy.heapoffmemory.rest;
/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import au.com.bytecode.opencsv.CSVReader;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.config.CacheConfiguration;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.directmemory.DirectMemory;
import org.apache.directmemory.cache.CacheService;
import org.apache.directmemory.measures.Sizing;
import org.apache.directmemory.memory.Pointer;
import org.olamy.heapoffmemory.model.BusStop;
import org.olamy.heapoffmemory.service.BusStopService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author Olivier Lamy
 */
@Service( "busStopService#default" )
public class DefaultBusStopService
    implements BusStopService
{

    private static final Logger log = LoggerFactory.getLogger( DefaultBusStopService.class.getName() );

    Cache cache;

    CacheService<Integer, BusStop> cacheService;

    int threadNumber = Integer.getInteger( "threadNumber", 3 );

    ExecutorService executorService = Executors.newFixedThreadPool( threadNumber );

    boolean multiThreadedLoad = Boolean.getBoolean( "multiThreadedLoad" );

    public void loadAll()
        throws Exception
    {

        InputStream is =
            Thread.currentThread().getContextClassLoader().getResourceAsStream( "star_arret_physique.csv" );

        CSVReader csvReader = new CSVReader( new InputStreamReader( is ), ';' );

        List<String[]> lines = csvReader.readAll();

        boolean useDm = StringUtils.equals( "dm", System.getProperty( "cacheFramework" ) );

        AtomicInteger counter = new AtomicInteger();

        //int counter = 0;

        printMemory( "before objects creation " );

        if ( useDm )
        {
            cacheService = new DirectMemory<Integer, BusStop>().setNumberOfBuffers(
                Integer.getInteger( "directMemory.numberOfBuffers", 22000 ) ).setSize(
                Integer.getInteger( "directMemory.size", 2000 ) ).setInitialCapacity(
                Integer.getInteger( "directMemory.initialCapacity",
                                    DirectMemory.DEFAULT_INITIAL_CAPACITY ) ).setConcurrencyLevel(
                Integer.getInteger( "directMemory.concurrencyLevel", threadNumber + 1 ) ).newCacheService();
        }
        else
        {
            CacheConfiguration cacheConfiguration = new CacheConfiguration();
            cacheConfiguration.setName( "foo" );

            // ehcache heap off configuration
            if ( Boolean.getBoolean( "ehcache.heapoff" ) )
            {
                cacheConfiguration.setOverflowToOffHeap( true );
                cacheConfiguration.setMaxBytesLocalHeap( Long.valueOf( 100 * 1024 * 1024 ) );
                cacheConfiguration.setMaxBytesLocalOffHeap( Long.valueOf( 100 * 1024 * 1024 ) );
            }
            else
            {
                cacheConfiguration.setMaxEntriesLocalHeap( 20000 );
            }
            cache = new Cache( cacheConfiguration );
            CacheManager.getInstance().addCache( cache );
        }

        for ( String[] line : lines )
        {

            BusStop busStop = new BusStop();

            //log.debug( "line {}", Arrays.asList( line ) );
            // 0     1    2   3       4      5      6       7       8         9         10      11       12   13        14
            //TIMEO;TYPE;NOM;C_INSEE;X_CC48;Y_CC48;X_WGS84;Y_WGS84;ACCES_PMR;LI_ACCESS;LI_NORM;MOBILIER;BANC;ECLAIRAGE;POUBELLE
            //1001;mobilier;Longs Champs;35238;1355395,088800000000000;1355395,088800000000000;-1,632530197157740;48,129076710012299;OUI;1;;Abri double;OUI;OUI;NON
            busStop.id = line[0];
            busStop.type = line[1];
            busStop.name = line[2];
            busStop.inseeCode = line[3];
            busStop.handicappedAccess = line[8];

            busStop.furnitures = line[11];
            busStop.seating = BooleanUtils.toBoolean( line[12] );
            busStop.light = BooleanUtils.toBoolean( line[13] );
            busStop.trash = BooleanUtils.toBoolean( line[14] );
            busStop.lines = generateLines();

            if ( useDm )
            {
                putInDMCache( busStop, counter );
            }
            else
            {
                putInEhcache( busStop, counter );
            }


        }

        if ( !useDm )
        {
            log.info( " ehcache inMemorySize: {}, OffHeapSize: {}",
                      new Long[]{ cache.calculateInMemorySize(), cache.calculateOffHeapSize() } );
        }

        executorService.shutdown();

        while ( !executorService.isTerminated() )
        {
            Thread.sleep( 1000 );
        }

        printMemory( "after cache put   " + counter.get() + " " );
    }


    private List<String> generateLines()
    {
        List<String> lines = new ArrayList<String>();

        for ( int i = 0; i < 100; i++ )
        {
            lines.add( Long.toString( new Date().getTime() ) );
        }

        return lines;
    }

    private void putInEhcache( BusStop busStop, AtomicInteger counter )
        throws Exception
    {
        EhCacheLoaderThread ehCacheLoaderThread = new EhCacheLoaderThread( cache, busStop, counter );
        if ( multiThreadedLoad )
        {
            executorService.submit( ehCacheLoaderThread );
        }
        else
        {
            ehCacheLoaderThread.run();
        }

    }

    private void putInDMCache( BusStop busStop, AtomicInteger counter )
        throws Exception
    {
        DmCacheLoaderThread dmCacheLoaderThread = new DmCacheLoaderThread( cacheService, busStop, counter );
        if ( multiThreadedLoad )
        {
            executorService.submit( dmCacheLoaderThread );
        }
        else
        {
            dmCacheLoaderThread.run();
        }


    }

    private void printMemory( String message )
    {
        System.gc();

        long freeMemory = Runtime.getRuntime().freeMemory();

        long totalMemory = Runtime.getRuntime().totalMemory();

        log.info( "{} freeMemory: {} Mb, totalMemory: {} Mb",
                  new String[]{ message, Sizing.inMb( freeMemory ), Sizing.inMb( totalMemory ) } );
    }

    static class DmCacheLoaderThread
        implements Runnable
    {

        final CacheService<Integer, BusStop> cacheService;

        final BusStop busStop;

        final AtomicInteger counter;

        DmCacheLoaderThread( CacheService<Integer, BusStop> cacheService, BusStop busStop, AtomicInteger counter )
        {
            this.cacheService = cacheService;
            this.counter = counter;
            this.busStop = busStop;
        }

        @Override
        public void run()
        {
            int value = counter.getAndIncrement();
            Pointer<BusStop> pointer = cacheService.put( value, busStop );
            if ( pointer == null )
            {
                throw new RuntimeException( "pointer is null for counter: " + value );
            }
            log.debug( "put element {} in dmcache", value );
        }
    }

    static class EhCacheLoaderThread
        implements Runnable
    {
        final Cache cache;

        final BusStop busStop;

        final AtomicInteger counter;


        EhCacheLoaderThread( Cache cache, BusStop busStop, AtomicInteger counter )
        {
            this.cache = cache;
            this.busStop = busStop;
            this.counter = counter;
        }

        @Override
        public void run()
        {
            int value = counter.getAndIncrement();
            Element element = new Element( value, busStop );
            log.debug( "put element {} in ehcache", value );
            cache.put( element );
        }
    }

}
